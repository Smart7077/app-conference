package uz.nuu.conference;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppConferenceServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppConferenceServerApplication.class, args);
    }

}
