package uz.nuu.conference.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import uz.nuu.conference.entity.Attachment;
import uz.nuu.conference.entity.AttachmentContent;
import uz.nuu.conference.entity.AttachmentType;
import uz.nuu.conference.entity.enums.AttachmentTypeEnum;
import uz.nuu.conference.exception.BadRequestException;
import uz.nuu.conference.payload.ApiResponse;
import uz.nuu.conference.repository.AttachmentContentRepository;
import uz.nuu.conference.repository.AttachmentRepository;
import uz.nuu.conference.repository.AttachmentTypeRepository;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Iterator;
import java.util.UUID;

@Service
public class AttachmentService {

    final
    AttachmentTypeRepository attachmentTypeRepository;
    final
    AttachmentRepository attachmentRepository;
    final
    AttachmentContentRepository attachmentContentRepository;

    public AttachmentService(AttachmentTypeRepository attachmentTypeRepository, AttachmentRepository attachmentRepository, AttachmentContentRepository attachmentContentRepository) {
        this.attachmentTypeRepository = attachmentTypeRepository;
        this.attachmentRepository = attachmentRepository;
        this.attachmentContentRepository = attachmentContentRepository;
    }

    public ApiResponse saveFile(MultipartHttpServletRequest request) throws IOException {
        Iterator<String> fileNames = request.getFileNames();
        MultipartFile file = request.getFile(fileNames.next());

        if (!request.getParameter("type").equals("undefined")) {
            AttachmentType attachmentType = attachmentTypeRepository.findByAttachmentTypeEnum(AttachmentTypeEnum.valueOf(request.getParameter("type")));
            if (!attachmentType.getContentType().contains(file.getContentType())) {
                return new ApiResponse("File ning tipi " + attachmentType.getContentType() + " lardan biri bo'lishi shart ", false);
            }
            if (file.getContentType().contains("image")) {
                BufferedImage image = ImageIO.read(file.getInputStream());
                if (image.getWidth() <= attachmentType.getWidth() && image.getHeight() <= attachmentType.getHeight()) {
                    return new ApiResponse("Rasmning hajmi " + attachmentType.getWidth() + "x" + attachmentType.getHeight() + " bo'lishi shart", false);
                }
                if (file.getSize() > attachmentType.getSize()) {
                    return new ApiResponse("Rasm hajmi " + attachmentType.getSize() + " dan oshmasligi kerak", false);
                }
            }

            Attachment attachment = new Attachment(
                    file.getOriginalFilename(),
                    file.getContentType(),
                    file.getSize()
            );
            Attachment savedAttachment = attachmentRepository.save(attachment);

            try {
                AttachmentContent attachmentContent = new AttachmentContent(
                        savedAttachment,
                        file.getBytes()
                );
                attachmentContentRepository.save(attachmentContent);
                return new ApiResponse("Saccessfully saved", true, savedAttachment.getId());

            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }
        return new ApiResponse("Fayl tipi noma'lum", false);
    }

    public HttpEntity<?> getFile(UUID id) {
        Attachment attachment = attachmentRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Get Attachment"));
        AttachmentContent attachmentContent = attachmentContentRepository.findByAttachment(attachment);

        return ResponseEntity.ok().contentType(MediaType.valueOf(attachment.getContentType()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + attachment.getName() + "\"")
                .body(attachmentContent.getBytes());
    }

    public ApiResponse deleteEvent(UUID id) {
        try {
            Attachment attachment = attachmentRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getEvent"));
            AttachmentContent findAttachmentContent = attachmentContentRepository.findByAttachment(attachment);
            attachmentContentRepository.delete(findAttachmentContent);
            attachmentRepository.delete(attachment);
            return new ApiResponse("Deleted", true);
        } catch (Exception e) {
            return new ApiResponse("Error deleting !!!", false);
        }

    }
}
