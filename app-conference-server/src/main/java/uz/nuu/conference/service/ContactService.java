package uz.nuu.conference.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.nuu.conference.payload.ReqAwareAddress;
import uz.nuu.conference.payload.ReqContact;
import uz.nuu.conference.payload.ReqPhoneNumber;
import uz.nuu.conference.entity.AwareAddress;
import uz.nuu.conference.entity.Contact;
import uz.nuu.conference.entity.PhoneNumber;
import uz.nuu.conference.repository.*;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ContactService {

    @Autowired
    ContactRepository contactRepository;
    @Autowired
    DistrictRepository districtRepository;
    @Autowired
    PhoneNumberRepository phoneNumberRepository;
    @Autowired
    PhoneNumberTypeRepository phoneNumberTypeRepository;
    @Autowired
    AwareAddressRepository awareAddressRepository;
    @Autowired
    AwareRepository awareRepository;

    public Contact addContact(ReqContact reqContact, Contact oldContact) {
        try {
            Contact contact = new Contact();
            if (oldContact != null) {
                contact = oldContact;
            }
            contact.setDistrict(districtRepository.getOne(reqContact.getDistrictId()));
            contact.setHome(reqContact.getHome());
            contact.setStreet(reqContact.getStreet());
            contact.setWebsite(reqContact.getWebsite());
            Contact savedContact = contactRepository.save(contact);
            savePhoneNumbers(reqContact.getReqPhoneNumbers(), savedContact);
            saveAwareAddresses(reqContact.getReqAwareAddresses(), savedContact);
            return savedContact;

        } catch (Exception e) {
            return oldContact;
        }

    }

    public void savePhoneNumbers(List<ReqPhoneNumber> reqPhoneNumbers, Contact contact) {
        phoneNumberRepository.saveAll(reqPhoneNumbers.stream()
                .map(reqPhoneNumber -> savePhoneNumber(reqPhoneNumber, contact)).collect(Collectors.toList()));
    }

    public PhoneNumber savePhoneNumber(ReqPhoneNumber reqPhoneNumber, Contact contact) {
        return new PhoneNumber(
                phoneNumberTypeRepository.getOne(reqPhoneNumber.getPhoneNumberTypeId()),
                reqPhoneNumber.getNumber(),
                contact
        );
    }

    public void saveAwareAddresses(List<ReqAwareAddress> reqAwareAddresses, Contact contact) {
        awareAddressRepository.saveAll(reqAwareAddresses.stream()
                .map(reqAwareAddress -> saveAwareAddress(reqAwareAddress, contact)).collect(Collectors.toList()));
    }

    private AwareAddress saveAwareAddress(ReqAwareAddress reqAwareAddress, Contact contact) {
        return new AwareAddress(
                awareRepository.getOne(reqAwareAddress.getAwareId()),
                reqAwareAddress.getAddress(),
                contact
        );
    }
}
