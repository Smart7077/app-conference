package uz.nuu.conference.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import uz.nuu.conference.entity.*;
import uz.nuu.conference.entity.template.AbsEntity;
import uz.nuu.conference.payload.*;
import uz.nuu.conference.entity.enums.OperationName;
import uz.nuu.conference.repository.AttachmentRepository;
import uz.nuu.conference.repository.UniversityRepository;
import uz.nuu.conference.utils.CommonUtils;

import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class UniversityService {
    @Autowired
    UniversityRepository universityRepository;
    @Autowired
    AttachmentRepository attachmentRepository;
    @Autowired
    ContactService contactService;

//    public ApiResponse addUniversity(ReqUniversity reqUniversity) {
//        try {
//            University university = new University();
//            university.setName(reqUniversity.getName());
//            university.setEnabled(false);
//            university.setLogo(reqUniversity.getLogoId() == null ? null : attachmentRepository.getOne(reqUniversity.getLogoId()));
//            university.setPhotos(reqUniversity.getPhotosId() == null ? null : attachmentRepository.findAllById(reqUniversity.getPhotosId()));
//            university.setContact(contactService.addContact(reqUniversity.getReqContact(), reqUniversity.getId() == null ? null :
//                    university.getContact()));
//
//            University savedUniversity = universityRepository.save(university);
//            if (reqUniversity.getId() == null) {
//                universityLogRepository.save(new UniversityLog(
//                        reqUniversity.getId(),
//                        reqUniversity.getName(),
//                        OperationName.INSERT
//                ));
//            }
//            return new ApiResponse(reqUniversity.getId() == null ? "University saqlandi " : "University o'zgartirildi", true);
//        } catch (Exception e) {
//            return new ApiResponse("Saqlashda xatolik !!!", false);
//        }
//
//    }

//    public ApiResponse changeEnabled(UUID id, User user) {
//        try {
//            ApiResponse apiResponse = new ApiResponse();
//            apiResponse.setSuccess(true);
//            University university = universityRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Not found University By Id"));
//            if (user.getRoles().size() > 1 && university.isEnabled()) {
//                university.setEnabled(false);
//                apiResponse.setMessage("Universitet o'chirildi");
//            } else if (user.getRoles().size() > 0) {
//                apiResponse.setMessage(university.isEnabled() ? "Kompaniya bloklandi" : "Kompaniya blokdan chiqarildi !");
//                university.setEnabled(!university.isEnabled());
//            } else {
//                apiResponse.setSuccess(false);
//                apiResponse.setMessage("Kompaniya mavjud emas ");
//            }
//            universityRepository.save(university);
//            return apiResponse;
//        } catch (Exception e) {
//            return new ApiResponse("Kompaniya topilmadi !", false);
//        }
//    }

    public ResPageable getUniversities(int page, int size) {
        Pageable pageable = CommonUtils.getPageable(page, size);
        Page<University> universityPage = universityRepository.findAll(pageable);

        return new ResPageable(
                universityPage.getContent().stream().map(university -> new ResUniversity()).collect(Collectors.toList()),
                page,
                universityPage.getTotalPages(),
                universityPage.getTotalElements());
    }

//    public ResUniversity getUniversity(University university) {
//        return new ResUniversity(
//                university.getId(),
//                university.getNameUz(),
//                getContact(university.getContact()),
//                university.getLogo() == null ? null : university.getLogo().getId(),
//                university.getEvents().stream().map(AbsEntity::getId).collect(Collectors.toList()),
//                university.getPhotos().stream().map(AbsEntity::getId).collect(Collectors.toList()),
//                university.getVideo().stream().map(AbsEntity::getId).collect(Collectors.toList())
//        );
//    }

    public ResContact getContact(Contact contact) {
        return new ResContact(
                contact.getDistrict().getRegion().getCountry().getId(),
                contact.getDistrict().getRegion().getCountry().getNameUz(),
                contact.getDistrict().getRegion().getCountry().getNameRu(),
                contact.getDistrict().getRegion().getCountry().getNameEn(),
                contact.getDistrict().getRegion().getId(),
                contact.getDistrict().getRegion().getNameUz(),
                contact.getDistrict().getRegion().getNameRu(),
                contact.getDistrict().getRegion().getNameEn(),
                contact.getDistrict().getId(),
                contact.getDistrict().getNameUz(),
                contact.getDistrict().getNameRu(),
                contact.getDistrict().getNameEn(),
                contact.getHome(),
                contact.getStreet(),
                contact.getWebsite(),
                contact.getEmail(),
                contact.getPhoneNumbers().stream().map(this::getPhoneNumber).collect(Collectors.toList()),
                contact.getAwareAddresses().stream().map(this::getAwareAddress).collect(Collectors.toList())

        );
    }

    public ReqPhoneNumber getPhoneNumber(PhoneNumber phoneNumber) {

        return new ReqPhoneNumber(
                phoneNumber.getPhoneNumberType().getId(),
                phoneNumber.getPhoneNumberType().getNameUz(),
                phoneNumber.getPhoneNumberType().getNameRu(),
                phoneNumber.getPhoneNumberType().getNameEn(),
                phoneNumber.getNumber()
        );
    }

    public ReqAwareAddress getAwareAddress(AwareAddress awareAddress) {
        return new ReqAwareAddress(
                awareAddress.getAware().getId(),
                awareAddress.getAware().getNameUz(),
                awareAddress.getAware().getNameRu(),
                awareAddress.getAware().getNameEn(),
                awareAddress.getAddress()
        );
    }
}
