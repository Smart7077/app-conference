package uz.nuu.conference.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.nuu.conference.entity.Event;
import uz.nuu.conference.entity.User;
import uz.nuu.conference.payload.ApiResponse;
import uz.nuu.conference.payload.ReqEvent;
import uz.nuu.conference.payload.ResEvent;
import uz.nuu.conference.payload.ResIEvent;
import uz.nuu.conference.repository.AttachmentRepository;
import uz.nuu.conference.repository.EventRepository;

import java.util.List;

@Service
public class EventService {
    final
    AttachmentRepository attachmentRepository;
    @Autowired
    EventRepository eventRepository;

    public EventService(AttachmentRepository attachmentRepository) {
        this.attachmentRepository = attachmentRepository;
    }

    public ApiResponse addEvent(ReqEvent reqEvent) {
        try {
            Event event = new Event();
            event.setName(reqEvent.getName());
            event.setContent(reqEvent.getContent());
            event.setStartedDate(reqEvent.getStartedDate());
            event.setVideos(reqEvent.getVideosId() == null ? null : attachmentRepository.findAllById(reqEvent.getVideosId()));
            event.setPhotos(reqEvent.getPhotosId() == null ? null : attachmentRepository.findAllById(reqEvent.getPhotosId()));
            eventRepository.save(event);

            return new ApiResponse("Event saqlandi !", true);

        } catch (Exception e) {
            return new ApiResponse("Saqlashda xatolik !", false);
        }

    }

    public List<ResEvent> getMyAllEvents(User user) {
        List<ResIEvent> events = eventRepository.findAllByCreatedBy(user.getId());

        return null;
    }
}
