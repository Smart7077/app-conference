package uz.nuu.conference.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResEvent {
    UUID id;
    String name;
    String content;
    Date date;
    List<UUID> photosId;
    List<UUID> videosId;
}
