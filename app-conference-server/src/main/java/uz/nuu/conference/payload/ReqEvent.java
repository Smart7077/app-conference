package uz.nuu.conference.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReqEvent {
    private String name;
    private String content;
    private Date startedDate;
    private List<UUID> videosId;
    private List<UUID> photosId;
}
