package uz.nuu.conference.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReqUser {
    private UUID id;
    private Integer positionId;
    private String firstName;
    private String lastName;
    private String middleName;
    private String userName;
    private Date birthDate;
    private String email;
    private String passport;
    private String phoneNumber;
    private String password;
    private ReqContact reqContact;
    private Integer universityId;
}
