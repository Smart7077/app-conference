package uz.nuu.conference.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.nuu.conference.entity.AwareAddress;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResContact {

    private Integer countryId;
    private String countryUz;
    private String countryRu;
    private String countryEn;
    private Integer regionId;
    private String regionUz;
    private String regionRu;
    private String regionEn;
    private Integer districtId;
    private String districtUz;
    private String districtRu;
    private String districtEn;
    private String home;
    private String street;
    private String website;
    private String email;
    private List<ReqPhoneNumber> phoneNumbers;
    private List<ReqAwareAddress> reqAwareAddresses;

}
