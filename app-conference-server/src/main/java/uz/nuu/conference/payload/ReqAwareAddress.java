package uz.nuu.conference.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReqAwareAddress {
    private Integer awareId;
    private String awareUz;
    private String awareRu;
    private String awareEn;
    private String address;
}
