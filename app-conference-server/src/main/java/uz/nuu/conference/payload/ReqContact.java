package uz.nuu.conference.payload;

import lombok.Data;

import java.util.List;

@Data
public class ReqContact {
    List<ReqPhoneNumber> reqPhoneNumbers;
    List<ReqAwareAddress> reqAwareAddresses;
    private Integer id;
    private Integer districtId;
    private String street;
    private String home;
    private String website;


}
