package uz.nuu.conference.payload;

import lombok.Data;

import java.util.List;
import java.util.UUID;

@Data
public class ReqUniversity {
    private UUID id;
    private String name;
    private ReqContact reqContact;
    private UUID eventId;
    private Integer logoId;
    private List<Integer> photosId;
    private List<Integer> videosId;

}
