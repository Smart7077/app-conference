package uz.nuu.conference.payload;

import uz.nuu.conference.entity.Attachment;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public interface ResIEvent {
    UUID getId();

    String getName();

    String getContent();

    Date getStartedDate();

    Timestamp getDeadLine();

    List<Attachment> getPhotos();

    List<Attachment> getVideos();
}
