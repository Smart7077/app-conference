package uz.nuu.conference.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReqPhoneNumber {
    private Integer phoneNumberTypeId;
    private String phoneNumberTypeUz;
    private String phoneNumberTypeRu;
    private String phoneNumberTypeEn;
    private String number;
}
