package uz.nuu.conference.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResUniversity {
    private UUID id;
    private String name;
    private ResContact contact;
    private UUID logoId;
    private List<UUID> eventsId;
    private List<UUID> photosId;
    private List<UUID> videosId;
}
