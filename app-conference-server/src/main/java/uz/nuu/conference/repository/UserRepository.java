package uz.nuu.conference.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.nuu.conference.entity.User;

import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {
    Optional<User> findByPhoneNumber(String s);

    boolean existsByPhoneNumber(String phoneNumber);
}
