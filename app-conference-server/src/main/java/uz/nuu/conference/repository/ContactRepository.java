package uz.nuu.conference.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.nuu.conference.entity.Contact;

public interface ContactRepository extends JpaRepository<Contact,Integer> {
}
