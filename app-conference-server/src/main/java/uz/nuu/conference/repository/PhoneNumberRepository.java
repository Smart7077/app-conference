package uz.nuu.conference.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.nuu.conference.entity.PhoneNumber;

public interface PhoneNumberRepository extends JpaRepository<PhoneNumber,Integer> {
}
