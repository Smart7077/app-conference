package uz.nuu.conference.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.nuu.conference.entity.AttachmentType;
import uz.nuu.conference.entity.enums.AttachmentTypeEnum;

import java.util.UUID;

public interface AttachmentTypeRepository extends JpaRepository<AttachmentType, UUID> {
    AttachmentType findByAttachmentTypeEnum(AttachmentTypeEnum attachmentTypeEnum);
}
