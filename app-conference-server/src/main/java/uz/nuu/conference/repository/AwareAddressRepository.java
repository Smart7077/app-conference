package uz.nuu.conference.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.nuu.conference.entity.AwareAddress;

@RepositoryRestResource(path = "awareAddress")
public interface AwareAddressRepository extends JpaRepository<AwareAddress, Integer> {
}
