package uz.nuu.conference.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.nuu.conference.entity.PhoneNumberType;
import uz.nuu.conference.projection.CustomPhoneNumberType;

@RepositoryRestResource(path = "phoneNumberType", excerptProjection = CustomPhoneNumberType.class)
public interface PhoneNumberTypeRepository extends JpaRepository<PhoneNumberType, Integer> {
}
