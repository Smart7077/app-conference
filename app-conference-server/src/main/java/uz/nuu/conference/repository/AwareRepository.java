package uz.nuu.conference.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.nuu.conference.entity.Aware;
import uz.nuu.conference.projection.CustomAware;

@RepositoryRestResource(path = "aware", excerptProjection = CustomAware.class)
public interface AwareRepository extends JpaRepository<Aware, Integer> {
}
