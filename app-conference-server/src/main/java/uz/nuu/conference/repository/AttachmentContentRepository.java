package uz.nuu.conference.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.nuu.conference.entity.Attachment;
import uz.nuu.conference.entity.AttachmentContent;

import java.util.UUID;

public interface AttachmentContentRepository extends JpaRepository<AttachmentContent, UUID> {
    AttachmentContent findByAttachment(Attachment attachment);
}
