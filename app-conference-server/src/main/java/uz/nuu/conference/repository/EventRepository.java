package uz.nuu.conference.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.nuu.conference.entity.Event;
import uz.nuu.conference.payload.ResIEvent;
import uz.nuu.conference.projection.CustomEvent;

import java.util.List;
import java.util.UUID;

@RepositoryRestResource(excerptProjection = CustomEvent.class)
public interface EventRepository extends JpaRepository<Event, UUID> {

    List<ResIEvent> findAllByCreatedBy(UUID id);
}
