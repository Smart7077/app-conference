package uz.nuu.conference.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.nuu.conference.entity.University;
import uz.nuu.conference.projection.CustomUniversity;

import java.util.UUID;

@RepositoryRestResource(path = "university", excerptProjection = CustomUniversity.class)
public interface UniversityRepository extends JpaRepository<University, Integer> {
}
