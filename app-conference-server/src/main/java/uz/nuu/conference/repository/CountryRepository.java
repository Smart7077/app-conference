package uz.nuu.conference.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.nuu.conference.entity.Country;
import uz.nuu.conference.projection.CustomCountry;

@RepositoryRestResource(path = "country",excerptProjection = CustomCountry.class)
public interface CountryRepository extends JpaRepository<Country,Integer> {
}
