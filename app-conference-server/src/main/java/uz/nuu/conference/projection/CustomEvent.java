package uz.nuu.conference.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.nuu.conference.entity.Attachment;
import uz.nuu.conference.entity.Event;

import javax.xml.crypto.Data;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Projection(name = "customEvent", types = Event.class)
public interface CustomEvent {
    UUID getId();

    String getContent();

    Date getStartedDate();

    Timestamp getDeadline();

    List<UUID> getVideos();

    List<UUID> getPhotos();


}
