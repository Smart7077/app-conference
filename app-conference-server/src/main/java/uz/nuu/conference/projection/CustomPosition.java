package uz.nuu.conference.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.nuu.conference.entity.Position;

@Projection(name = "customProjection", types = Position.class)
public interface CustomPosition {
    Integer getId();

    String getNameUz();

    String getNameRu();

    String getNameEn();
}
