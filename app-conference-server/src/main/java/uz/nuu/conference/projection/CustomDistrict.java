package uz.nuu.conference.projection;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;
import uz.nuu.conference.entity.District;
import uz.nuu.conference.entity.Region;

@Projection(name = "customDistrict", types = District.class)
public interface CustomDistrict {
    Integer getId();

    String getNameUz();

    String getNameRu();

    String getNameEn();


    Region getRegion();

    @Value("#{target.region?.id}")
    Integer getRegionId();

}
