package uz.nuu.conference.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.nuu.conference.entity.University;

@Projection(name = "customUniversity", types = University.class)
public interface CustomUniversity {
    Integer getId();

    String getNameUz();

    String getNameRu();

    String getNameEn();

}
