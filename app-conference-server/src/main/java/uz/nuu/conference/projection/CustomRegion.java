package uz.nuu.conference.projection;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;
import uz.nuu.conference.entity.Region;


@Projection(name = "customRegion", types = Region.class)
public interface CustomRegion {
    Integer getId();

    String getNameUz();

    String getNameRu();

    String getNameEn();

    @Value("#{target.country?.id}")
    String getCountryId();

    @Value("#{target.country?.nameUz}")
    String getCountryNameUz();

    @Value("#{target.country?.nameRu}")
    String getCountryNameRu();

    @Value("#{target.country?.nameEn}")
    String getCountryNameEn();

}
