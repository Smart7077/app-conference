package uz.nuu.conference.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.nuu.conference.entity.Aware;

@Projection(name = "customAware", types = Aware.class)
public interface CustomAware {
    Integer getId();

    String getNameUz();

    String getNameRu();

    String getNameEn();
}
