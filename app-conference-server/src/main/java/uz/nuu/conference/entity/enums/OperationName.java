package uz.nuu.conference.entity.enums;

public enum OperationName {
    INSERT, UPDATE, DELETE
}
