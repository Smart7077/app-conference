package uz.nuu.conference.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.nuu.conference.entity.template.AbsNameEntity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class PhoneNumber extends AbsNameEntity {
    @ManyToOne(fetch = FetchType.LAZY)
    private PhoneNumberType phoneNumberType;

    private String number;

    @ManyToOne(fetch = FetchType.LAZY)
    private Contact contact;
}
