package uz.nuu.conference.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.nuu.conference.entity.enums.AttachmentTypeEnum;
import uz.nuu.conference.entity.template.AbsEntity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class AttachmentType extends AbsEntity {
    private String contentType;
    @Enumerated(value = EnumType.STRING)
    private AttachmentTypeEnum attachmentTypeEnum;
    private int width;
    private int height;
    private long size;
}
