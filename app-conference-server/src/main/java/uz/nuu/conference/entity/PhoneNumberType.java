package uz.nuu.conference.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import uz.nuu.conference.entity.template.AbsNameEntity;

import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class PhoneNumberType extends AbsNameEntity {
}
