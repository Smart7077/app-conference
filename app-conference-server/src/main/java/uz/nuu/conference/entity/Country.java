package uz.nuu.conference.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.nuu.conference.entity.template.AbsNameEntity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class Country extends AbsNameEntity {

    @OneToMany(fetch = FetchType.LAZY,mappedBy = "country",cascade = CascadeType.ALL)
    private List<Region> regions;
}
