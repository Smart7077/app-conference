package uz.nuu.conference.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.ManyToAny;
import uz.nuu.conference.entity.template.AbsEntity;
import uz.nuu.conference.entity.template.AbsNameEntity;

import javax.persistence.*;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "contact")
public class Contact extends AbsEntity {
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private District district;
    private String home;
    private String street;
    private String website;
    private String email;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "contact", cascade = CascadeType.ALL)
    private List<PhoneNumber>  phoneNumbers;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "contact", cascade = CascadeType.ALL)
    private List<AwareAddress> awareAddresses;

}
