package uz.nuu.conference.entity;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import uz.nuu.conference.entity.enums.RoleName;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.*;

@Data
@Entity
public class Role implements GrantedAuthority {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer Id;

    @Enumerated(value = EnumType.STRING)
    private RoleName name;

    @Override
    public String getAuthority() {
        return name.name();
    }
}
