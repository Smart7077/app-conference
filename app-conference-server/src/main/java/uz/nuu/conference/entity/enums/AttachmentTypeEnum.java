package uz.nuu.conference.entity.enums;

public enum AttachmentTypeEnum {
    LOGO,AVATAR,EVENT,ARTICLE,REPORT
}
