package uz.nuu.conference.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.nuu.conference.entity.template.AbsEntity;
import uz.nuu.conference.entity.template.AbsNameEntity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Event extends AbsEntity {

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String content;

    @Column(nullable = false)
    private Date startedDate;

    private Timestamp deadLine = new Timestamp(new Date().getTime());

    @OneToMany(fetch = FetchType.LAZY)
    private List<Attachment> videos;

    @OneToMany(fetch = FetchType.LAZY)
    private List<Attachment> photos;


}
