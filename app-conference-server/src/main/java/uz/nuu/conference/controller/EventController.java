package uz.nuu.conference.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uz.nuu.conference.entity.Event;
import uz.nuu.conference.entity.User;
import uz.nuu.conference.payload.ApiResponse;
import uz.nuu.conference.payload.ReqEvent;
import uz.nuu.conference.repository.EventRepository;
import uz.nuu.conference.security.CurrentUser;
import uz.nuu.conference.service.EventService;

import java.util.UUID;

@RestController
@RequestMapping("/api/event")
public class EventController {
    @Autowired
    EventService eventService;
    @Autowired
    EventRepository eventRepository;

    @PostMapping
    public HttpEntity<?> createEvent(@RequestBody ReqEvent reqEvent) {
        ApiResponse response = eventService.addEvent(reqEvent);
        return ResponseEntity.status(response.isSuccess() ? HttpStatus.CREATED : HttpStatus.CONFLICT).body(response);
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getEvent(@PathVariable UUID id) {
        Event event = eventRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getEvent"));
        return ResponseEntity.ok(event);
    }

    @GetMapping("/getMyEvents")
    public HttpEntity<?> getMyEvents(@CurrentUser User user) {
        return ResponseEntity.ok(new ApiResponse("all", true, eventRepository.findAllByCreatedBy(user.getId())));
    }
}
