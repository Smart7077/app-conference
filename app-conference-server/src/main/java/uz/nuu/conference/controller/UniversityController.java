package uz.nuu.conference.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import uz.nuu.conference.payload.ApiResponse;
import uz.nuu.conference.payload.ReqUniversity;
import uz.nuu.conference.entity.University;
import uz.nuu.conference.entity.User;
import uz.nuu.conference.repository.UniversityRepository;
import uz.nuu.conference.security.CurrentUser;
import uz.nuu.conference.service.UniversityService;
import uz.nuu.conference.utils.AppConstants;

import java.util.UUID;

@RestController
@RequestMapping("/api/university")
public class UniversityController {

//    @Autowired
//    UniversityRepository universityRepository;
//    @Autowired
//    UniversityService universityService;
//
//
//    @PostMapping
//    public HttpEntity<?> addUniversity(@RequestBody ReqUniversity reqUniversity) {
//        ApiResponse response = universityService.addUniversity(reqUniversity);
//        return ResponseEntity.status(response.isSuccess() ? HttpStatus.CREATED : HttpStatus.CONFLICT).body(response);
//
//    }
//
//    @DeleteMapping("/{id")
//    public HttpEntity<?> deleteUniversity(@PathVariable UUID id, @CurrentUser User user) {
//        ApiResponse response = universityService.changeEnabled(id, user);
//        return ResponseEntity.status(response.isSuccess() ? HttpStatus.OK : HttpStatus.CONFLICT).body(response);
//    }
//
//    @GetMapping("/{id}")
//    public HttpEntity<?> getUniversity(@PathVariable UUID id) {
//        University university = universityRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getUniversity"));
//        return ResponseEntity.ok(university);
//    }
//
//    @GetMapping
//    public HttpEntity<?> getUniversityPegeable(@RequestParam(value = "page", defaultValue = AppConstants.DEFAULT_PAGE) int page,
//                                               @RequestParam(value = "size", defaultValue = AppConstants.DEFAULT_SIZE) int size) {
//
//        return ResponseEntity.ok(universityService.getUniversities(page, size));
//    }
}
