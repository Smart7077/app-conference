package uz.nuu.conference.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import uz.nuu.conference.entity.User;
import uz.nuu.conference.repository.AttachmentTypeRepository;
import uz.nuu.conference.repository.RoleRepository;
import uz.nuu.conference.repository.UserRepository;

@Component
public class DataLoader implements CommandLineRunner {
    @Value("${spring.datasource.initialization-mode}")
    private String initialMode;
    final
    UserRepository userRepository;

    final
    PasswordEncoder passwordEncoder;

    final
    RoleRepository roleRepository;
    final
    AttachmentTypeRepository attachmentTypeRepository;

    public DataLoader(UserRepository userRepository, PasswordEncoder passwordEncoder, RoleRepository roleRepository, AttachmentTypeRepository attachmentTypeRepository) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.roleRepository = roleRepository;
        this.attachmentTypeRepository = attachmentTypeRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        if (initialMode.equals("always")) {
            userRepository.save(
                    new User(
                            "Ali",
                            "Valiyev",
                            "G'ani o'g'li",
                            "+998919687077",
                            "AB4551736",
                            passwordEncoder.encode("1"),
                            roleRepository.findAll()
                    ));
//            attachmentTypeRepository.save(new AttachmentType("image/jpeg"))

        }
    }
}
