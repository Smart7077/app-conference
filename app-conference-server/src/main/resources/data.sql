insert into role
values (10, 'ROLE_USER'),
       (20, 'ROLE_MANAGER'),
       (30, 'ROLE_DIRECTOR'),
       (40, 'ROLE_ADMIN');

INSERT INTO aware(name_uz, name_ru, name_en)
VALUES ('Telegram', 'Телеграм', 'Telegram'),
       ('Facebook', 'Фейсбук', 'Facebook');
insert into country(name_uz, name_ru, name_en)
values ('O`zbekiston', 'O`zbekiston', 'O`zbekiston');
-- Viloyatlar
insert into region(country_id, name_uz, name_ru, name_en)
values (1, 'ТОШКЕНТ ШАҲАР', 'ТОШКЕНТ ШАҲАР', 'ТОШКЕНТ ШАҲАР');
insert into region(country_id, name_uz, name_ru, name_en)
values (1, 'ТОШКЕНТ ВИЛОЯТИ', 'ТОШКЕНТ ВИЛОЯТИ', 'ТОШКЕНТ ВИЛОЯТИ');
insert into region(country_id, name_uz, name_ru, name_en)
values (1, 'АНДИЖОН ВИЛОЯТИ', 'АНДИЖОН ВИЛОЯТИ', 'АНДИЖОН ВИЛОЯТИ');
insert into region(country_id, name_uz, name_ru, name_en)
values (1, 'БУХОРО ВИЛОЯТИ', 'БУХОРО ВИЛОЯТИ', 'БУХОРО ВИЛОЯТИ');
insert into region(country_id, name_uz, name_ru, name_en)
values (1, 'ЖИЗЗАХ ВИЛОЯТИ', 'ЖИЗЗАХ ВИЛОЯТИ', 'ЖИЗЗАХ ВИЛОЯТИ');
insert into region(country_id, name_uz, name_ru, name_en)
values (1, 'ҚАШҚАДАРЁ ВИЛОЯТИ', 'ҚАШҚАДАРЁ ВИЛОЯТИ', 'ҚАШҚАДАРЁ ВИЛОЯТИ');
insert into region(country_id, name_uz, name_ru, name_en)
values (1, 'НАВОИЙ ВИЛОЯТИ', 'НАВОИЙ ВИЛОЯТИ', 'НАВОИЙ ВИЛОЯТИ');
insert into region(country_id, name_uz, name_ru, name_en)
values (1, 'НАМАНГАН ВИЛОЯТИ', 'НАМАНГАН ВИЛОЯТИ', 'НАМАНГАН ВИЛОЯТИ');
insert into region(country_id, name_uz, name_ru, name_en)
values (1, 'САМАРҚАНД ВИЛОЯТИ', 'САМАРҚАНД ВИЛОЯТИ', 'САМАРҚАНД ВИЛОЯТИ');
insert into region(country_id, name_uz, name_ru, name_en)
values (1, 'СУРХОНДАРЁ ВИЛОЯТИ', 'СУРХОНДАРЁ ВИЛОЯТИ', 'СУРХОНДАРЁ ВИЛОЯТИ');
insert into region(country_id, name_uz, name_ru, name_en)
values (1, 'СИРДАРЁ ВИЛОЯТИ', 'СИРДАРЁ ВИЛОЯТИ', 'СИРДАРЁ ВИЛОЯТИ');
insert into region(country_id, name_uz, name_ru, name_en)
values (1, 'ФАРҒОНА ВИЛОЯТИ', 'ФАРҒОНА ВИЛОЯТИ', 'ФАРҒОНА ВИЛОЯТИ');
insert into region(country_id, name_uz, name_ru, name_en)
values (1, 'ХОРАЗМ ВИЛОЯТИ', 'ХОРАЗМ ВИЛОЯТИ', 'ХОРАЗМ ВИЛОЯТИ');
insert into region(country_id, name_uz, name_ru, name_en)
values (1, 'ҚОРАҚАЛПОҒИСТОН РЕСП', 'ҚОРАҚАЛПОҒИСТОН РЕСП', 'ҚОРАҚАЛПОҒИСТОН РЕСП');

insert into position(name_uz, name_ru, name_en)
values ('Prof', 'Prof', 'Prof');
insert into position(name_uz, name_ru, name_en)
values ('Prof Dr', 'Prof Dr', 'Prof Dr');
insert into position(name_uz, name_ru, name_en)
values ('Dr', 'Dr', 'Dr');
insert into position(name_uz, name_ru, name_en)
values ('Mr', 'Mr', 'Mr');
insert into position(name_uz, name_ru, name_en)
values ('Ms', 'Ms', 'Ms');
insert into position(name_uz, name_ru, name_en)
values ('Mrs', 'Mrs', 'Mrs');

insert into district(region_id, name_uz, name_ru, name_en)
values (1, 'Yakkasaroy tumani', 'Yakkasaroy tumani', 'Yakkasaroy tumani');
insert into phone_number_type(name_uz, name_ru, name_en)
values ('Ish', 'rabochiy', 'Work');
insert into phone_number_type(name_uz, name_ru, name_en)
values ('Uy', 'Domashniy', 'Home');

insert into university(name_en, name_ru, name_uz)
values ('National University of Uzbekistan', 'Национальный университет Узбекистана', 'O`zbekiston Milliy Universiteti');
insert into university(name_en, name_ru, name_uz)
values ('Tashkent state university of economics', 'Toshkent davlat iqtisodiyot universiteti', 'Toshkent davlat iqtisodiyot universiteti');


-- To'lov turlarini qo'shish
-- insert into pay_type(name_uz,name_ru,name_en)values ('Naqd','Наличие','Cash'),('Pul o''tkazish','Перечесление','Transfer');
