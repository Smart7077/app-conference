// ref: https://umijs.org/config/

import {resolve} from "path";


export default {
  treeShaking: true,
  // routes: [
  //   {
  //     path: '/',
  //     component: '../layouts/index',
  //     routes: [
  //       { path: '/', component: '../pages/index' }
  //     ]
  //   }
  // ],
  plugins: [
    // ref: https://umijs.org/plugin/umi-plugin-react.html
    ['umi-plugin-react', {
      antd: true,
      dva: true,
      dynamicImport: {webpackChunkName: true},
      title: 'Conference',
      dll: false,
      routes: {
        exclude: [
          /models\//,
          /services\//,
          /model\.([tj])sx?$/,
          /service\.([tj])sx?$/,
          /components\//,
        ],
      },



    }],
  ],
  proxy: {
    "/api": {
      "target": "http://localhost/",
      "changeOrigin": true
    }
  },
  alias: {
    api: resolve(__dirname, './src/services/'),
    utils: resolve(__dirname, "./src/utils"),
    services: resolve(__dirname, "./src/services"),
    components: resolve(__dirname, "./src/component"),
    config: resolve(__dirname, "./src/utils/config"),
  },
  outputPath: '../app-conference-server/src/main/resources/static'

}
