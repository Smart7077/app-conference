import api from "api";
import {config} from "utils"
import {router} from "umi";
import {toast} from "react-toastify";

const {
  addEvent
} = api


export default {
  namespace: 'event',
  state: {
    collapsed: false,
  },
  subscriptions: {},
  effects: {

  }

  ,
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
}
