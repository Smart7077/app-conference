import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {toast} from "react-toastify";
import {Button, Card, Col, DatePicker, Form, Image, Input, Row} from "antd";
import {Label} from "reactstrap";
import {connect} from "react-redux";
@connect(({app}) => ({app}))
class Add extends Component {


  render() {

    const {dispatch, app} = this.props
    const {photosId, videosId, photosList, logoId, typeName, videoList} = app;

    const changePhotoList = (v) => {
      console.log(v)
      dispatch({
        type: 'app/updateState',
        payload: {
          photosList: v.fileList
        }
      })
    }
    const changeVideoList = (v) => {
      dispatch({
        type: 'app/updateState',
        payload: {
          videoList: v.fileList
        }
      })
    }

    const saveEvent = (v) => {
      let reqEvent = {...v, photosId, videosId};
      dispatch({
        type: 'app/addEvent',
        payload: {
          ...reqEvent
        }
      })
    }

    const fileUpLoad = (e) => {

      let typeName = e.target.files[0].type;
      if (e.target.name === 'photosId') {
        if (typeName === 'image/jpeg' || typeName === 'image/jpg' || typeName === 'image/png') {

          dispatch({
            type: 'app/uploadFile',
            payload: {
              file: e.target.files[0],
              fileUpload: true,
              type: 'LOGO'
            }
          }).then(res => {
            if (res.success) {
              console.log(res)
              let photos = [...photosId];
              photos.push(res.object)
              dispatch({
                type: 'app/updateState',
                payload: {
                  photosId: photos
                }
              })
            }
          })
        } else {
          toast.error("File tipi mos emas !")
        }
      } else {
        if (typeName === 'video/mp4') {
          dispatch({
            type: 'app/uploadFile',
            payload: {
              file: e.target.files[0],
              fileUpload: true,
              type: 'EVENT'
            }
          }).then(res => {
            if (res.success) {
              console.log(res)
              let videos = [...videosId];
              videos.push(res.object)
              dispatch({
                type: 'app/updateState',
                payload: {
                  videosId: videos
                }
              })
            }
          })
        } else {
          toast.error('File tipi mos emas !');
        }

      }
      console.log(videosId)
      console.log(photosId)
    }

    return (

      <div className="text-center" style={{backgroundColor: 'white'}}>
        <Image style={{
          padding: -50
        }
        } src="/img/q8.jpg"/>
        <Row>
          <Col push={1} offset={2} span={18}>
            <Card className="mx-5">
              <h2>Host Your Event</h2>
              <Form wrapperCol={{span: 22}} initialValues={{remember: true}}
                    className="mt-5" name="basic" onFinish={saveEvent}>
                <div className="ml-5">
                  <Row>
                    <Col span={8} offset={2}>
                      <Form.Item name="name">
                        <Input size="large" name="name" placeholder="Event Theme"/>
                      </Form.Item>
                    </Col>
                    <Col span={10}>
                      <Form.Item name="startedDate">
                        <DatePicker name="startedDate" size="large"
                                    placeholder="Select event date"/>
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row>
                    <Col span={18} offset={2}>
                      <Form.Item name="content">
                        <Input.TextArea name="content" placeholder="Content"/>
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row className="mt-5">
                    <Col offset={2} span={8}>
                      <Form.Item name="photos">
                        <div className="custom-file">
                          <Label className="custom-file-label text-left" htmlFor="companyLogo">Event Photos</Label>
                          <Input id="companyLogo" className="custom-file-input" type="file" style={{border: '0'}}
                                 onChange={fileUpLoad} name="photosId"/>
                        </div>

                        {/*<Upload name="photos" onChange={changePhotoList}>*/}
                        {/*  <Button icon={<UploadOutlined/>}>Upload You Event Photos</Button>*/}
                        {/*</Upload>*/}
                      </Form.Item>
                    </Col>
                  </Row>
                  <Row className="mt-5">
                    <Col offset={2} span={8}>
                      <Form.Item name="videos">
                        <div className="custom-file">
                          <Label className="custom-file-label text-left" htmlFor="companyPhoto">Event Videos</Label>
                          <Input type="file" id="companyPhoto" className="custom-file-input" style={{border: '0'}}
                                 onChange={fileUpLoad}
                                 name="videosId"/>
                        </div>

                      </Form.Item>
                    </Col>
                  </Row>
                </div>
                <Button type="primary" htmlType="submit">Save</Button>
              </Form>
            </Card>
          </Col>
        </Row>
      </div>
    )
      ;
  }

}

Add.propTypes = {};

export default Add;
