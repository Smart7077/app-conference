import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow} from "@material-ui/core";
import {Button, Card, Col, Divider, Image, List, message, Modal, Row, Typography} from "antd";
import {Link, router} from "umi";
import {connect} from "react-redux";
import Meta from "antd/es/card/Meta";
import Text from "antd/es/typography/Text";
import Title from "antd/es/typography/Title";
import Paragraph from "antd/es/typography/Paragraph";
import ListItem from "antd/es/upload/UploadList/ListItem";
import ReactPlayer from "react-player";
import Typical from 'react-typical'

@connect(({app, cabinet}) => ({app, cabinet}))
class Cabinet extends Component {

  componentDidMount() {
    const {dispatch} = this.props;
    dispatch({
      type: 'app/getEvents'
    })
  }

  render() {

    const {dispatch, app, cabinet} = this.props;
    const {events} = app;
    const {isModalVisible, currentVideo} = cabinet;

    const handleChange = (item) => {
      dispatch({
        type: 'cabinet/updateState',
        payload: {
          currentVideo: item
        }
      })
      dispatch({
        type: 'cabinet/updateState',
        payload: {
          isModalVisible: true
        }
      })
    }
    const handleOk = () => {
      dispatch({
        type: 'cabinet/updateState',
        payload: {
          isModalVisible: false
        }
      })
    }
    const handleDelete = (item) => {
      dispatch({
        type:'cabinet/delete',
        payload:{

        }
      })
    }

    return (
      <div className="mt-5" style={{backgroundColor: 'white'}}>
        <Button className="float-right" type="primary" onClick={() => router.push('/event/add')}>Add
          Event</Button>
        <br/>
        <h1 className="d-flex justify-content-center">Your Events</h1>
        {events.length > 0 ?
          events.map((item, i) =>
            <Card title="Event" size={"small"}>
              <Row className="mt-5">
                <Col push={1} span={6}>
                  <Card
                    cover={<img src="img/q15.jpg"/>}
                  >
                    {item.videos.length > 0 ?
                      item.videos.map((item, i) =>
                        <List>
                          <List.Item key={i}>
                            <Typography.Link onClick={() => handleChange(item)}>
                              {item.name}
                            </Typography.Link>
                          </List.Item>
                        </List>
                      )
                      : ''

                    }

                    <Meta title={item.name}/>
                    < Button size="large" type="primary">Watch
                    </Button><br/>
                    <Button type="dashed">Download</Button>
                  </Card>
                </Col>
                <Col push={2} span={12}>
                  <Typography>
                    <Title>Content</Title>
                    <Paragraph>
                      {item.content}
                    </Paragraph>
                    <Title>Photos</Title>
                    <Row>
                      {item.photos.length > 0 ?
                        item.photos.map((photo, i) =>
                          <Image className="mx-2" style={{height: '10vh', width: '50px'}}
                                 src={`/api/attach/${photo.id}`}/>
                        ) : <h3>Rasm yo'q</h3>
                      }
                    </Row>
                    <Title>Speaker</Title>
                    <Paragraph>Johm Wuick</Paragraph>
                    <Title>Date</Title>
                    <Paragraph>{new Date(item.startedDate).toLocaleString()}</Paragraph>
                  </Typography>
                </Col>
                <Col push={1} span={4}>
                  <Button style={{borderRadius: '15%'}} className="mx-2" type="primary">Edit</Button>
                  <Button style={{borderRadius: '15%', backgroundColor: 'red', color: 'white'}}
                          onClick={() => handleDelete(item)}>Delete</Button>
                </Col>
                <Divider/>
              </Row>

            </Card>
          )
          : <h1>Event topilmadi !</h1>}

        <Modal className="modalChange" title="Basic Modal" visible={isModalVisible} onOk={handleOk} onCancel={handleOk}>
          <div className="d-flex justify-content-center">
            {currentVideo.id ?
              <ReactPlayer
                className="modalChange"
                controls
                volume={1}
                url={`/api/attach/${currentVideo.id}`}
              />
              : 'Not Found'}
          </div>
        </Modal>

      </div>

    );
  }
}

Cabinet.propTypes = {};

export default Cabinet;
