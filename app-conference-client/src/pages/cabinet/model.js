import api from "api";
import {config} from "utils"
import {router} from "umi";
import {toast} from "react-toastify";
import {STORAGE_NAME} from "@/utils/constant";

const {deleteEvent} = api


export default {
  namespace: 'cabinet',
  state: {
    collapsed: false,
    isModalVisible: false,
    currentVideo: ''
  },
  subscriptions: {},
  effects: {
    * delete({payload}, {call, put}) {
      const res = yield call(deleteEvent, payload);
      if (res.success) {
        console.log(res);
      }
    }
  },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
}
