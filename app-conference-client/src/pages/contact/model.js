import api from "api";
import {config} from "utils"
import {router} from "umi";
import {toast} from "react-toastify";
import {STORAGE_NAME} from "@/utils/constant";

const {signUp, signIn} = api


export default {
  namespace: 'contact',
  state: {
    phone: "+998919687077",
  },
  subscriptions: {},
  effects: {

  },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
}
