import React, {Component, useEffect} from 'react';
import PropTypes from 'prop-types';
import {Avatar, Button, Card, Col, Divider, Form, Image, Input, Row, Space} from "antd";
import {PhoneFilled, PhoneOutlined, UserOutlined} from "@ant-design/icons";
import Text from "antd/es/typography/Text";
import {connect} from "react-redux";

const {TextArea} = Input;

function Contact(props) {
  useEffect(() => {
    console.log("ishladi")
  })
  console.log(props)
  return (
    <div style={{backgroundColor: 'white'}}>
      <Image src="/img/q16.jpg"/>
      <Divider/>
      <Row className="mt-5">
        <Col span={20} push={2}>
          <Row>
            <Col span={9}>
              <Avatar size={50} src="img/q5.png" icon={<UserOutlined/>}/>
              <Text id="1" className="ml-3 font-weight-bolder">+998998887766</Text>
            </Col>
            <Col span={9} push={5}>
              <Avatar size={50} src="img/q6.png" icon={<UserOutlined/>}/>
              <Text id="1" className="ml-3 font-weight-bolder">conferenceExample@gmail.com</Text>
            </Col>
          </Row>


          <Divider/>
          <Card>
            <h1 className="font-weight-bolder">Thank you for you interested in Conference</h1>
            <Form className="mt-5">
              <Row>
                <Col span={8} push={1}>
                  <Form.Item label="Your name">
                    <Input size="large" style={{borderRadius: 5}}/>
                  </Form.Item>
                </Col>
                <Col span={8} push={5}>
                  <Form.Item label="Email">
                    <Input size="large" style={{borderRadius: 5}}/>
                  </Form.Item>
                </Col>
              </Row>

              <Form.Item label="Subject" labelCol={{span: 2}}>
                <Input size="large" style={{borderRadius: 5}}/>
              </Form.Item>
              <Form.Item className="mt-5" labelCol={{span: 2}} label="Message">
                <TextArea size="large" style={{borderRadius: 5}}/>
              </Form.Item>
              <Button style={{borderRadius: 5}} type="default">Submit</Button>
            </Form>
          </Card>
        </Col>
      </Row>
    </div>
  )
};
export default connect(({contact}) => ({contact}))(Contact);
