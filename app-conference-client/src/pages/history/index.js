import React, {Component} from 'react';
import PropTypes from 'prop-types';
import ReactPlayer from "react-player";
import {connect} from "react-redux";
import {Card, Col, Image, Row, Space, Typography} from "antd";
import Title from "antd/es/typography/Title";
import Paragraph from "antd/es/typography/Paragraph";
import Gallery from 'react-grid-gallery';
import {Spa} from "@material-ui/icons";

@connect(({app}) => ({app}))
class History extends Component {
  componentDidMount() {
    const {dispatch} = this.props;
    dispatch({
      type: 'app/getEvents'
    })
  }

  render() {

    const {dispatch, app} = this.props;
    const {events} = app;
    console.log(events);
    const IMAGES =
      [
        {
          src: "/img/q16.jpg",
          thumbnail: "/img/q16.jpg",
          thumbnailWidth: 320,
          thumbnailHeight: 174,
          isSelected: true,
        },
        {
          src: "/img/q17.jpg",
          thumbnail: "/img/q17.jpg",
          thumbnailWidth: 320,
          thumbnailHeight: 174,
          isSelected: true,
        },
        {
          src: "/img/q18.jpg",
          thumbnail: "/img/q18.jpg",
          thumbnailWidth: 320,
          thumbnailHeight: 174,
          isSelected: true,
        },
        {
          src: "/img/q16.jpg",
          thumbnail: "/img/q16.jpg",
          thumbnailWidth: 320,
          thumbnailHeight: 174,
          isSelected: true,
        },
        {
          src: "/img/q17.jpg",
          thumbnail: "/img/q17.jpg",
          thumbnailWidth: 320,
          thumbnailHeight: 174,
          isSelected: true,
        },
        {
          src: "/img/q18.jpg",
          thumbnail: "/img/q18.jpg",
          thumbnailWidth: 320,
          thumbnailHeight: 174,
          isSelected: true,
        },
        {
          src: "/img/q17.jpg",
          thumbnail: "/img/q17.jpg",
          thumbnailWidth: 320,
          thumbnailHeight: 174,
          isSelected: true,
        },
        {
          src: "/img/q18.jpg",
          thumbnail: "/img/q18.jpg",
          thumbnailWidth: 320,
          thumbnailHeight: 174,
          isSelected: true,
        }


      ]


    return (
      <div style={{backgroundColor: 'white'}} className="mb-5">
        <Image src="/img/q20.jpg"/>

        <Card>
          <Typography>
            <Title>Conferece Histoty</Title>
            <Paragraph>
              The International Conference on Advanced Research in Social Sciences (ICARSS) is the industry’s leading
              event, with a history of success. Its 2019 edition took place on the 7th-9th of March, 2019 at the
              University of London in London, United Kingdom.
            </Paragraph>
            <Paragraph>
              Over 155 attendees from 20+ countries – Vietnam, Kenya, Kuwait, Canada, Mexico, Slovakia, Ghana, and many
              others – gathered at the event to address common challenges and engage in discussions with peers on dozens
              of subjects. Presentations and workshops included such relevant issues as educational and international
              comparisons of fundamental values of different systems, outpatient treatment, economic benefits of waste
              management, personnel motivation, and many others.
            </Paragraph>
            <Paragraph>
              During the course of the three days of the event, members of the academia, scholars, researchers,
              scientists, and non-profit and private sector representatives attended 30+ lectures and workshops. With
              presentations delivered by some of the most respected academics, ICARSS 2019 received strong positive
              feedback from the attendees. The guided tour of the city of London further added to the experience of the
              audience as we explored the city’s hidden gems and discovered its majestic architecture.
            </Paragraph>
          </Typography>
          <Gallery images={IMAGES}/>
        </Card>
      </div>
    )
      ;
  }

}

History.propTypes = {};

export default History;
