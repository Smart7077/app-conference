import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from "react-redux";
import {Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow} from "@material-ui/core";
import {Form, Input, Button} from "antd";
import {AvField, AvForm} from 'availity-reactstrap-validation'
import {Modal, ModalBody, ModalHeader} from "reactstrap";

@connect(({app, country}) => ({app, country}))
class Country extends Component {
  componentDidMount() {
    const {dispatch} = this.props;
    dispatch({
      type: 'app/getCountries'
    });
  }


  state = {
    isModalVisible: false,
    openDeleteModal: false
  }

  render() {
    const {dispatch, app, country} = this.props;
    const {countries} = app;
    const {currentCountry, deletedId} = country;
    const {isModalVisible, openDeleteModal} = this.state;

    const showModal = (item) => {
      dispatch({
        type: 'country/updateState',
        payload: {
          currentCountry: item
        }
      })
      this.setState({isModalVisible: true})
    };
    const showAddModal = () => {
      console.log(currentCountry)
      this.setState({isModalVisible: true})
    }
    const handleSave = (e, v) => {
      console.log(v)
      if (currentCountry.id) {
        dispatch({
          type: 'country/editCountry',
          payload: {
            path: currentCountry.id,
            ...v
          }
        })
      } else {
        dispatch({
          type: 'country/saveCountry',
          payload: {
            ...v
          }
        })
      }
      dispatch({
        type: 'country/updateState',
        payload: {
          currentCountry: ''
        }
      })

      this.setState({isModalVisible: false})
    };
    const handleCancel = () => {
      dispatch({
        type: 'country/updateState',
        payload: {
          currentCountry: ''
        }
      })
      this.setState({isModalVisible: false})
    };
    const deleteCountry = () => {
      dispatch({
        type: 'country/delete',
        payload: {
          id: deletedId
        }
      })
      this.setState({openDeleteModal: false})
    }
    const handleOpenModal = () => {
      this.setState({isModalVisible: true})
    }
    const handleDeleteModalOpen = (id) => {
      dispatch({
        type: 'country/updateState',
        payload: {
          deletedId: id
        }
      });
      this.setState({openDeleteModal: true})
    }
    const handleCancelDeleteModal = () => {
      this.setState({openDeleteModal: false})
    }


    return (
      <div style={{backgroundColor: 'white'}}>
        <TableContainer component={Paper}>
          <Button type="primary" onClick={handleOpenModal} style={{float: 'right'}}>Add</Button>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>T/R</TableCell>
                <TableCell>NameUz</TableCell>
                <TableCell>NameRu</TableCell>
                <TableCell>NameEn</TableCell>
                <TableCell>Action</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {countries.length != 0 ?
                countries.map((item, i) =>
                  <TableRow key={item.id}>
                    <TableCell>{i + 1}</TableCell>
                    <TableCell>{item.nameUz}</TableCell>
                    <TableCell>{item.nameRu}</TableCell>
                    <TableCell>{item.nameEn}</TableCell>
                    <TableCell>
                      <TableRow>
                        <TableCell><Button variant="contained" color="primary"
                                           onClick={() => showModal(item)}>Edit</Button></TableCell>
                        <TableCell><Button variant="contained" color="secondary"
                                           onClick={() => handleDeleteModalOpen(item.id)}>Delete</Button></TableCell>
                      </TableRow>
                    </TableCell>
                  </TableRow>
                ) : ''}

            </TableBody>
          </Table>
        </TableContainer>

        <div>
          <Modal className="mt-5" isOpen={isModalVisible} toggle={showModal}>
            <ModalHeader>
              <h2>Add country</h2>
            </ModalHeader>
            <ModalBody>
              <AvForm onValidSubmit={handleSave}>
                <AvField defaultValue={currentCountry.nameUz} placeholder="NameUz" name="nameUz"/>
                <AvField defaultValue={currentCountry.nameRu} placeholder="NameRu" name="nameRu"/>
                <AvField defaultValue={currentCountry.nameEn} placeholder="NameEn" name="nameEn"/>
                <button className="btn btn-light" onClick={handleCancel}>Cancel</button>
                <button type="submit" className="btn btn-success">Save</button>
              </AvForm>
            </ModalBody>
          </Modal>
          <Modal className="mt-5" isOpen={openDeleteModal} toggle={handleDeleteModalOpen}>
            <ModalHeader>
              <h2>Delete country</h2>
            </ModalHeader>
            <ModalBody>
              <Button type="default" onClick={handleCancelDeleteModal}>Cancel</Button>
              <Button type="primary" onClick={deleteCountry}>Delete</Button>
            </ModalBody>
          </Modal>

        </div>
      </div>


    );
  }
}

Country.propTypes =
  {}
;

export default Country;
