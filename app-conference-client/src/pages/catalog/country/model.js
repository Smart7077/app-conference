import api from 'api'

const {editCountry, addCountry, deleteCountry} = api
export default {
  namespace: 'country',
  state: {
    currentCountry: '',
    nameUz: '',
    nameRu: '',
    nameEn: '',
    deletedId: ''
  },
  subscriptions: {},
  effects: {
    * editCountry({payload}, {call, put}) {
      const res = yield call(editCountry, payload)
      if (res.success) {
        yield put({
          type: 'app/getCountries'
        })
      }
    },
    * saveCountry({payload}, {call, put}) {
      const res = yield call(addCountry, payload);
      if (res.success) {
        yield put({
          type: 'app/getCountries'
        })
      }
    },
    * delete({payload}, {call, put}) {
      const res = yield call(deleteCountry, payload);
      if (res.success) {
        yield put({
          type: 'app/getCountries'
        })
      }
    }
  },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }


}
