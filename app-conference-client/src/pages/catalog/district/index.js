import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow} from "@material-ui/core";
import {Button} from "antd";
import {connect} from "react-redux";
import {Modal, ModalBody, ModalHeader} from "reactstrap";
import {AvField, AvForm} from "availity-reactstrap-validation";

@connect(({app, district}) => ({app, district}))
class District extends Component {
  componentDidMount() {
    const {dispatch} = this.props
    dispatch({
      type: 'app/getDistricts'
    })
    dispatch({
      type: 'app/getRegions'
    })
  };

  state = {
    isModalVisible: false,
    openDeleteModal: false
  }

  render() {
    const {dispatch, app, district} = this.props;
    const {districts, regions} = app;
    const {currentDistrict, deletedId} = district;
    const {isModalVisible, openDeleteModal} = this.state;

    const showModal = (item) => {
      dispatch({
        type: 'district/updateState',
        payload: {
          currentDistrict: item
        }
      })
      this.setState({isModalVisible: true})
    };
    const showAddModal = () => {
      console.log(currentDistrict)
      this.setState({isModalVisible: true})
    }
    const handleSave = (e, v) => {
      let id = v.regionId;
      console.log(id)
      if (currentDistrict.id) {
        dispatch({
          type: 'district/edit',
          payload: {
            path: currentDistrict.id,
            ...v
          }
        })
      } else {
        dispatch({
          type: 'district/add',
          payload: {
            ...v
          }
        })
      }
      dispatch({
        type: 'district/updateState',
        payload: {
          currentDistrict: ''
        }
      })

      this.setState({isModalVisible: false})
    };
    const handleCancel = () => {
      dispatch({
        type: 'district/updateState',
        payload: {
          currentDistrict: ''
        }
      })
      this.setState({isModalVisible: false})
    };
    const deleteCountry = () => {
      dispatch({
        type: 'district/delete',
        payload: {
          id: deletedId
        }
      })
      this.setState({openDeleteModal: false})
    }
    const handleOpenModal = () => {
      this.setState({isModalVisible: true})
    }
    const handleDeleteModalOpen = (id) => {
      dispatch({
        type: 'district/updateState',
        payload: {
          deletedId: id
        }
      });
      this.setState({openDeleteModal: true})
    }
    const handleCancelDeleteModal = () => {
      this.setState({openDeleteModal: false})
    }
    return (
      <div style={{backgroundColor: 'white'}}>
        <TableContainer component={Paper}>
          <Button type="primary" onClick={handleOpenModal} style={{float: 'right'}}>Add</Button>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>T/R</TableCell>
                <TableCell>NameUz</TableCell>
                <TableCell>NameRu</TableCell>
                <TableCell>NameEn</TableCell>
                <TableCell>Region</TableCell>
                <TableCell>Action</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {districts.length != 0 ?
                districts.map((item, i) =>
                  <TableRow key={item.id}>
                    <TableCell>{i + 1}</TableCell>
                    <TableCell>{item.nameUz}</TableCell>
                    <TableCell>{item.nameRu}</TableCell>
                    <TableCell>{item.nameEn}</TableCell>
                    <TableCell>{item.region.nameUz}</TableCell>
                    <TableCell>
                      <TableRow>
                        <TableCell><Button variant="contained" color="primary"
                                           onClick={() => showModal(item)}>Edit</Button></TableCell>
                        <TableCell><Button variant="contained" color="secondary"
                                           onClick={() => handleDeleteModalOpen(item.id)}>Delete</Button></TableCell>
                      </TableRow>
                    </TableCell>
                  </TableRow>
                ) : ''}

            </TableBody>
          </Table>
        </TableContainer>

        <div>
          <Modal className="mt-5" isOpen={isModalVisible} toggle={showModal}>
            <ModalHeader>
              <h2>Add District</h2>
            </ModalHeader>
            <ModalBody>
              <AvForm onValidSubmit={handleSave}>
                <AvField defaultValue={currentDistrict.nameUz} placeholder="NameUz" name="nameUz"/>
                <AvField defaultValue={currentDistrict.nameRu} placeholder="NameRu" name="nameRu"/>
                <AvField defaultValue={currentDistrict.nameEn} placeholder="NameEn" name="nameEn"/>
                <AvField type="select" placeholder="Country" name="region"
                         defaultValue={currentDistrict.region ? currentDistrict.region.nameUz : ''}>
                  {regions.map((item, i) =>
                    <option key={i}
                            value={`/api/region/${item.id}`}>{item.nameUz}</option>
                  )}
                </AvField>
                <button type="button" className="btn btn-light" onClick={handleCancel}>Cancel</button>
                <button type="submit" className="btn btn-success">Save</button>
              </AvForm>
            </ModalBody>
          </Modal>
          <Modal className="mt-5" isOpen={openDeleteModal} toggle={handleDeleteModalOpen}>
            <ModalHeader>
              <h2>Delete country</h2>
            </ModalHeader>
            <ModalBody>
              <Button type="default" onClick={handleCancelDeleteModal}>Cancel</Button>
              <Button type="primary" onClick={deleteCountry}>Delete</Button>
            </ModalBody>
          </Modal>

        </div>
      </div>
    );
  }
}

District.propTypes = {};

export default District;
