import api from "api";
import {config} from "utils"
import {router} from "umi";
import {toast} from "react-toastify";

const {
  editDistrict,
  deleteDistrict,
  addDistrict
} = api


export default {
  namespace: 'district',
  state: {
    collapsed: false,
    deletedId: '',
    currentDistrict: ''

  },
  subscriptions: {},
  effects: {
    * edit({payload}, {call, put}) {
      const res = yield call(editDistrict, payload);
      if (res.success) {
        yield put({
          type: 'app/getDistricts'
        })
      }
    },
    * add({payload}, {call, put}) {
      const res = yield call(addDistrict, payload)
      if (res.success) {
        yield put({
          type: 'app/getDistricts'
        })
      }
    },
    * delete({payload}, {call, put}) {
      const res = yield call(deleteDistrict, payload);
      if (res.success) {
        yield put({
          type: 'app/getDistricts'
        })
      }
    }
  },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
}
