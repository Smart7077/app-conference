import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow} from "@material-ui/core";
import {Button} from "antd";
import {connect} from "react-redux";
import {Modal, ModalBody, ModalHeader} from "reactstrap";
import {AvField, AvForm} from "availity-reactstrap-validation";

@connect(({app, region}) => ({app, region}))
class Region extends Component {

  componentDidMount() {
    const {dispatch} = this.props
    dispatch({
      type: 'app/getRegions'
    });
    dispatch({
      type: 'app/getCountries'
    });
  };

  state = {
    isModalVisible: false,
    openDeleteModal: false
  }

  render() {
    const {dispatch, app, region} = this.props;
    const {regions, countries} = app;
    const {currentRegion, deletedId} = region;
    const {isModalVisible, openDeleteModal} = this.state;

    const showModal = (item) => {
      console.log(item)
      dispatch({
        type: 'region/updateState',
        payload: {
          currentRegion: item
        }
      })
      this.setState({isModalVisible: true})
    };
    const showAddModal = () => {
      console.log(currentRegion)
      this.setState({isModalVisible: true})
    }
    const handleSave = (e, v) => {
      console.log(v)
      if (currentRegion.id) {
        dispatch({
          type: 'region/edit',
          payload: {
            path: currentRegion.id,
            ...v
          }
        })
      } else {
        dispatch({
          type: 'region/add',
          payload: {
            ...v
          }
        })
      }
      dispatch({
        type: 'region/updateState',
        payload: {
          currentRegion: ''
        }
      })

      this.setState({isModalVisible: false})
    };
    const handleCancel = () => {
      dispatch({
        type: 'region/updateState',
        payload: {
          currentRegion: ''
        }
      })
      this.setState({isModalVisible: false})
    };
    const deleteCountry = () => {
      dispatch({
        type: 'region/delete',
        payload: {
          id: deletedId
        }
      })
      this.setState({openDeleteModal: false})
    }
    const handleOpenModal = () => {
      this.setState({isModalVisible: true})
    }
    const handleDeleteModalOpen = (id) => {
      dispatch({
        type: 'region/updateState',
        payload: {
          deletedId: id
        }
      });
      this.setState({openDeleteModal: true})
    }
    const handleCancelDeleteModal = () => {
      this.setState({openDeleteModal: false})
    }
    return (
      <div style={{backgroundColor: 'white'}}>
        <TableContainer component={Paper}>
          <Button type="primary" onClick={handleOpenModal} style={{float: 'right'}}>Add</Button>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>T/R</TableCell>
                <TableCell>NameUz</TableCell>
                <TableCell>NameRu</TableCell>
                <TableCell>NameEn</TableCell>
                <TableCell>Country</TableCell>
                <TableCell>Action</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {regions.length != 0 ?
                regions.map((item, i) =>
                  <TableRow key={item.id}>
                    <TableCell>{i + 1}</TableCell>
                    <TableCell>{item.nameUz}</TableCell>
                    <TableCell>{item.nameRu}</TableCell>
                    <TableCell>{item.nameEn}</TableCell>
                    <TableCell>{item.countryNameUz}</TableCell>
                    <TableCell>
                      <TableRow>
                        <TableCell><Button variant="contained" color="primary"
                                           onClick={() => showModal(item)}>Edit</Button></TableCell>
                        <TableCell><Button variant="contained" color="secondary"
                                           onClick={() => handleDeleteModalOpen(item.id)}>Delete</Button></TableCell>
                      </TableRow>
                    </TableCell>
                  </TableRow>
                ) : ''}

            </TableBody>
          </Table>
        </TableContainer>

        <div>
          <Modal className="mt-5" isOpen={isModalVisible} toggle={showModal}>
            <ModalHeader>
              <h2>Add Region</h2>
            </ModalHeader>
            <ModalBody>
              <AvForm onValidSubmit={handleSave}>
                <AvField defaultValue={currentRegion.nameUz} placeholder="NameUz" name="nameUz"/>
                <AvField defaultValue={currentRegion.nameRu} placeholder="NameRu" name="nameRu"/>
                <AvField defaultValue={currentRegion.nameEn} placeholder="NameEn" name="nameEn"/>
                <AvField type="select" placeholder="Country" name="country"
                         value={currentRegion.country}>
                  {countries.map((item, i) =>
                    <option key={i}
                            value={`/api/country/${item.id}`}>{item.nameUz}</option>
                  )}
                </AvField>
                {/*<AvField name="countryId" type="select" value={currentRegion ? currentRegion.countryId : ''}>*/}
                {/*  <option>Select Country</option>*/}
                {/*  {countries.map(item =>*/}
                {/*    <option key={item.id} value={item.id}>{item.nameUz}</option>*/}
                {/*  )}*/}
                {/*</AvField>*/}

                <button className="btn btn-light" onClick={handleCancel}>Cancel</button>
                <button type="submit" className="btn btn-success">Save</button>
              </AvForm>
            </ModalBody>
          </Modal>
          <Modal className="mt-5" isOpen={openDeleteModal} toggle={handleDeleteModalOpen}>
            <ModalHeader>
              <h2>Delete country</h2>
            </ModalHeader>
            <ModalBody>
              <Button type="default" onClick={handleCancelDeleteModal}>Cancel</Button>
              <Button type="primary" onClick={deleteCountry}>Delete</Button>
            </ModalBody>
          </Modal>

        </div>
      </div>
    );
  }
}

Region.propTypes =
  {}
;

export default Region;
