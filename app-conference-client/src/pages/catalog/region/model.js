import api from "api";
import {config} from "utils"
import {router} from "umi";
import {toast} from "react-toastify";

const {
  editRegion,
  deleteRegion,
  addRegion
} = api


export default {
  namespace: 'region',
  state: {
    collapsed: false,
    deletedId: '',
    currentRegion: ''

  },
  subscriptions: {},
  effects: {
    * edit({payload}, {call, put}) {
      const res = yield call(editRegion, payload);
      if (res.success) {
        yield put({
          type: 'app/getRegions'
        })
      }
    },
    * add({payload}, {call, put}) {
      const res = yield call(addRegion, payload)
      if (res.success) {
        yield put({
          type: 'app/getRegions'
        })
      }
    },
    * delete({payload}, {call, put}) {
      const res = yield call(deleteRegion, payload);
      if (res.success) {
        yield put({
          type: 'app/getRegions'
        })
      }
    }
  },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
}
