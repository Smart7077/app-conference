import React, {Component} from 'react';
import {Card, Button, Col, Row, Form, Input, DatePicker, Alert, Space, Divider} from "antd";
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import {Select} from 'antd';
import {Option} from "antd/es/mentions";
import ArchiveIcon from '@material-ui/icons/Archive';
import {Icon} from "@material-ui/core";
import {MinusCircleOutlined, PlusOutlined} from '@ant-design/icons';


@connect(({app, auth}) => ({app, auth}))
class Registr extends Component {
  componentDidMount() {
    const {dispatch, app} = this.props;
    dispatch({
      type: 'app/getCountries',
    });
    dispatch({
      type: 'app/getPositions'
    });
    dispatch({
      type: 'app/getPhoneNumberTypes'
    })
    dispatch({
      type: 'app/getUniversities'
    })
    dispatch({
      type: 'app/getAwares'
    })
  }

  render() {

    const {dispatch, app, auth} = this.props;
    const {countries, positions, regions, districts, phoneNumberTypes, universities, awares} = app;
    const {form} = Form;
    const changeInput = (e, v) => {
    }
    const signUp = (v) => {
      let reqContact = {...v};
      let reqUser = {...v, reqContact};
      dispatch({
        type: 'auth/signUp',
        payload: {
          ...reqUser
        }
      })
      console.log(v)
    }
    const getRegionsByCountry = (v) => {
      dispatch({
        type: 'app/getRegionByCountry',
        payload: {
          id: v
        }
      })
    }
    const getDictrictsByRegion = (v) => {
      dispatch({
        type: 'app/getDistrictsByRegion',
        payload: {
          id: v
        }
      })
    }

    const selectBefore = (
      <Select defaultValue="http://" className="select-before">
        <Option value="http://">http://</Option>
        <Option value="https://">https://</Option>
      </Select>
    );
    const selectAfter = (
      <Select defaultValue=".uz" className="select-after">
        <Option value=".uz">.uz</Option>
        <Option value=".com">.com</Option>
        <Option value=".jp">.jp</Option>
        <Option value=".cn">.cn</Option>
        <Option value=".org">.org</Option>
      </Select>
    );


    return (
      <Row>
        <Col push={2} span={20}>
          <Card className="mx-5" style={{textAlign: 'center'}}>
            <h2 className="ml-1 font-weight-bold">Sign up</h2><br/>

            <Form name="basic" wrapperCol={{span: 22}} initialValues={{remember: true}}
                  onFinish={signUp}>
              <div className="ml-5">
                <Row>
                  <Col offset={2} span={9}>
                    <Form.Item name="positionId">
                      <Select placeholder="Position">
                        <Select.Option>Select Position</Select.Option>
                        {positions.length != 0 ?
                          positions.map((item, i) =>
                            <Select.Option key={i} value={item.id}>{item.nameUz}</Select.Option>
                          ) : ''}
                      </Select>
                    </Form.Item>
                  </Col>
                  <Col span={9}>
                    <Form.Item name="userName">
                      <Input style={{float: 'right'}} placeholder="User name" onChange={changeInput} name="userName"/>
                    </Form.Item>
                  </Col>
                </Row>
                <Row>
                  <Col offset={2} span={6}>
                    <Form.Item name="firstName">
                      <Input placeholder="First Name" onChange={changeInput} name="firstName"/>
                    </Form.Item>
                  </Col>
                  <Col span={6}>
                    <Form.Item name="lastName">
                      <Input placeholder="Last name" onChange={changeInput} name="lastName"/>
                    </Form.Item>
                  </Col>
                  <Col span={6}>
                    <Form.Item name="middleName">
                      <Input placeholder="Middle name" onChange={changeInput} name="middleName"/>
                    </Form.Item>
                  </Col>
                </Row>
                <Row>
                  <Col offset={2} span={6}>
                    <Form.Item name="birthDate">
                      <DatePicker placeholder="Birth Date " style={{float: 'left'}} size="middle" onChange={changeInput}
                                  name="birthDate"/>
                    </Form.Item>
                  </Col>
                  <Col span={6}>
                    <Form.Item name="email">
                      <Input placeholder="Email" onChange={changeInput} type="email" name="email"/>
                    </Form.Item>
                  </Col>
                  <Col span={6}>
                    <Form.Item name="passport">
                      <Input placeholder="Passport" onChange={changeInput} name="passport"/>
                    </Form.Item>
                  </Col>
                </Row>

                <Row>
                  <Col offset={2} span={9}>
                    <Form.Item name="phoneNumber">
                      <Input placeholder="Phone" required onChange={changeInput} name="phoneNumber"/>
                    </Form.Item>
                  </Col>
                  <Col span={9}>
                    <Form.Item name="password">
                      <Input.Password placeholder="Password" onChange={changeInput} name="password"/>
                    </Form.Item>
                  </Col>
                </Row>

                <h2 className="font-weight-bolder">Contact Detail</h2>
                <Row className="mt-5">
                  <Col offset={2} span={6}>
                    <Form.Item required name="countryId">
                      <Select onChange={getRegionsByCountry} placeholder="Country">
                        {countries.length != 0 ?
                          countries.map((item, i) =>
                            <Select.Option key={i} value={item.id}>{item.nameUz}</Select.Option>
                          ) :
                          ''}
                      </Select>
                    </Form.Item>
                  </Col>
                  <Col span={6}>
                    <Form.Item name="regionId">
                      <Select onChange={getDictrictsByRegion} placeholder="Region">
                        {regions.length != 0 ?
                          regions.map((item, i) =>
                            <Select.Option key={i} value={item.id}>{item.nameUz}</Select.Option>
                          ) : ''}

                      </Select>
                    </Form.Item>
                  </Col>
                  <Col span={6}>
                    <Form.Item name="districtId">
                      <Select placeholder="District">
                        {districts.length != 0 ?
                          districts.map((item, i) =>
                            <Select.Option key={i} value={item.id}>{item.nameUz}</Select.Option>
                          ) : ''}
                      </Select>
                    </Form.Item>
                  </Col>
                </Row>
                <Row>
                  <Col offset={2} span={18}>
                    <Form.Item name="universityId">
                      <Select placeholder="University">
                        {universities.length != 0 ?
                          universities.map((item, i) =>
                            <Select.Option key={item.id} value={item.id}>{item.nameUz}</Select.Option>
                          )
                          : ''}
                      </Select>
                    </Form.Item>
                  </Col>
                </Row>
                <Row>
                  <Col offset={2} span={8}>
                    <Form.Item name="street">
                      <Input placeholder="Street" onChange={changeInput} name="street"/>
                    </Form.Item>
                  </Col>
                  <Col span={8}>
                    <Form.Item name="home">
                      <Input placeholder="Home" onChange={changeInput} name="home"/>
                    </Form.Item>
                  </Col>
                </Row>
                <Row>
                  <Col offset={2} span={10}>
                    <Form.Item name="website">
                      <Input style={{marginBottom: 16}} addonBefore={selectBefore} addonAfter={selectAfter}
                             placeholder="Website" type="website"
                             onChange={changeInput} name="website"/>
                    </Form.Item>
                  </Col>
                </Row>


                <h3>Phone Numbers</h3>
                <Form.List name="reqPhoneNumbers">
                  {(fields, {add, remove}) => (
                    <>
                      {fields.map(field => (

                        <Row>
                          <Col offset={2} span={8}>
                            <Form.Item
                              {...field}
                              name={[field.name, 'phoneNumberTypeId']}
                              fieldKey={[field.fieldKey, 'phoneNumberTypeId']}
                              rules={[{required: true, message: 'Missing number type'}]}
                            >
                              <Select placeholder="Number type">
                                {phoneNumberTypes.length != 0 ?
                                  phoneNumberTypes.map((item, i) =>
                                    <Select.Option key={item.id} value={item.id}>{item.nameUz}</Select.Option>
                                  )
                                  : ''}
                              </Select>
                            </Form.Item>
                          </Col>
                          <Col span={8}>
                            <Form.Item
                              {...field}
                              name={[field.name, 'number']}
                              fieldKey={[field.fieldKey, 'number']}
                              rules={[{required: true, message: 'Missing number'}]}
                            >
                              <Input placeholder="Last Name"/>
                            </Form.Item>
                          </Col>
                          <Col span={2}>
                            <MinusCircleOutlined onClick={() => remove(field.name)}/>
                          </Col>
                        </Row>


                      ))}
                      <Row>
                        <Col offset={2}>
                          <Form.Item wrapperCol={25} style={{float: 'right'}}>
                            <Button type="dashed" onClick={() => add()} block icon={<PlusOutlined/>}>
                              Add Phone Number
                            </Button>
                          </Form.Item>
                        </Col>
                      </Row>

                    </>
                  )}
                </Form.List>

                <h3>Aware address</h3>
                <Form.List name="reqAwareAddresses">
                  {(fields, {add, remove}) => (
                    <>
                      {fields.map(field => (

                        <Row>
                          <Col offset={2} span={8}>
                            <Form.Item
                              {...field}
                              name={[field.name, 'awareId']}
                              fieldKey={[field.fieldKey, 'awareId']}
                              rules={[{required: true, message: 'Missing aware'}]}
                            >
                              <Select placeholder="Aware">
                                {awares.length != 0 ?
                                  awares.map((item, i) =>
                                    <Select.Option key={item.id} value={item.id}>{item.nameUz}</Select.Option>
                                  )
                                  : ''}
                              </Select>
                            </Form.Item>
                          </Col>
                          <Col span={8}>
                            <Form.Item
                              {...field}
                              name={[field.name, 'address']}
                              fieldKey={[field.fieldKey, 'address']}
                              rules={[{required: true, message: 'Missing address'}]}
                            >
                              <Input placeholder="AwareAddress"/>
                            </Form.Item>
                          </Col>
                          <Col span={2}>
                            <MinusCircleOutlined onClick={() => remove(field.name)}/>
                          </Col>
                        </Row>


                      ))}
                      <Row>
                        <Col offset={2}>
                          <Form.Item wrapperCol={25} style={{float: 'right'}}>
                            <Button type="dashed" onClick={() => add()} block icon={<PlusOutlined/>}>
                              Add Aware address
                            </Button>
                          </Form.Item>
                        </Col>
                      </Row>

                    </>
                  )}
                </Form.List>

                <Divider/>
                <Button type="primary" htmlType="submit">Create free account</Button>
              </div>
            </Form>
          </Card>
        </Col>
      </Row>


    );
  }
}

Registr.propTypes = {};

export default Registr;
