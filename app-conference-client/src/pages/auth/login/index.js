import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Button, Card, Col, Form, Input, Row} from "antd";
import {connect} from "react-redux";

@connect(({auth}) => ({auth}))
class Login extends Component {
  render() {
    const {dispatch,auth} = this.props;

    const login = (v) => {
      dispatch({
        type:'auth/login',
        payload:{
          ...v
        }

      })
    }
    return (
      <Row>
        <Col span={12} offset={4}>
          <Card className="text-center">
            <h2 className="t">Cabinet</h2><br/>
            <Form labelCol={{span:8}} initialValues={{remember: true}} wrapperCol={{span:14}} onFinish={login}>
              <Form.Item label="Phone Number" name="phoneNumber">
                <Input name="firstName"/>
              </Form.Item>
              <Form.Item label="Password" name="password">
                <Input.Password name="lastName"/>
              </Form.Item>
              <Button type="primary" htmlType="submit">Kirish</Button>
            </Form>
          </Card>
        </Col>
      </Row>
    );
  }
}

Login.propTypes = {};

export default Login;
