import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Card, Col, Divider, Image, Row} from "antd";
import Text from "antd/es/typography/Text";

class News extends Component {
  render() {
    return (
      <div style={{backgroundColor: 'white'}}>
        <Image src="img/q1.jpg"/>
        <Divider/>
        <Row>
          <Col flex='auto'>
            <Card>
              <Row>
                <Col span={8} push={1}><Image style={{height: '30vh', width: '250px'}} src="img/q22.jpg"/></Col>
                <Col span={12}>
                  <Text>
                    <h3>Milliy universitet</h3>
                    Tour of Duke Humfrey’s Library Duke Humfrey’s Library is the oldest reading room in the Bodleian
                    Library at the University of Oxford. Until 2015, it functioned primarily as a reading room for maps,
                    music, and pre-1641 rare books; This Library was used as the Hogwarts Library in the Harry Potter
                    ...

                  </Text></Col>
              </Row><br/>
              <Row>
                <Col span={8} push={1}><Image style={{height: '30vh', width: '250px'}} src="img/q22.jpg"/></Col>
                <Col span={12}>
                  <Text>
                    <h3>Milliy universitet</h3>
                    Tour of Duke Humfrey’s Library Duke Humfrey’s Library is the oldest reading room in the Bodleian
                    Library at the University of Oxford. Until 2015, it functioned primarily as a reading room for maps,
                    music, and pre-1641 rare books; This Library was used as the Hogwarts Library in the Harry Potter
                    ...
                  </Text></Col>
              </Row>
              <br/>
              <Row>
                <Col span={8} push={1}><Image style={{height: '30vh', width: '250px'}} src="img/q22.jpg"/></Col>
                <Col span={12}>
                  <Text>
                    <h3>Milliy universitet</h3>
                    Tour of Duke Humfrey’s Library Duke Humfrey’s Library is the oldest reading room in the Bodleian
                    Library at the University of Oxford. Until 2015, it functioned primarily as a reading room for maps,
                    music, and pre-1641 rare books; This Library was used as the Hogwarts Library in the Harry Potter
                    ...
                  </Text></Col>
              </Row>
              <br/>
              <Row>
                <Col span={8} push={1}><Image style={{height: '30vh', width: '250px'}} src="img/q22.jpg"/></Col>
                <Col span={12}>
                  <Text>
                    <h3>Milliy universitet</h3>
                    Tour of Duke Humfrey’s Library Duke Humfrey’s Library is the oldest reading room in the Bodleian
                    Library at the University of Oxford. Until 2015, it functioned primarily as a reading room for maps,
                    music, and pre-1641 rare books; This Library was used as the Hogwarts Library in the Harry Potter
                    ...
                  </Text></Col>
              </Row>
              <br/>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

News.propTypes = {};

export default News;
