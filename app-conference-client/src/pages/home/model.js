import api from "api";
import {config} from "utils"
import {router} from "umi";
import {toast} from "react-toastify";
import {STORAGE_NAME} from "@/utils/constant";

const {signUp, signIn} = api


export default {
  namespace: 'home',
  state: {
    collapsed: false,
  },
  subscriptions: {},
  effects: {
    * signUp({payload}, {call, put, select}) {
      console.log(payload)
      const res = yield call(signUp, payload);
      if (res.success) {
        router.push("/cabinet")
        toast.success(res.message);
      } else {
        toast.error(res.message)
      }
    },
    * login({payload}, {call, put}) {
      console.log(payload);
      const res = yield call(signIn, payload);
      console.log(res)
      if (res.success) {
        localStorage.setItem(STORAGE_NAME, res.tokenType + " " + res.tokenBody)
        router.push('/cabinet')
        toast.success("Sayt test rejimida ishlamoqda !");
      }
    },

  },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
}
