import React, {Component, PureComponent} from 'react';
import PropTypes from 'prop-types';
import {connect} from "react-redux";
import {Avatar, Breadcrumb, Button, Card, Col, Divider, Image, Layout, Menu, Row, Typography} from "antd";
import Title from "antd/es/typography/Title";
import Paragraph from "antd/es/typography/Paragraph";
import {router} from "umi";
import Text from "antd/es/typography/Text";
import Icon, {AccountBookOutlined, ClockCircleOutlined, UserOutlined} from "@ant-design/icons";
import * as url from "url";
import Meta from "antd/es/card/Meta";
import FlipNumbers from 'react-flip-numbers';

const {Header, Sider, Content} = Layout;

class Home extends PureComponent {
  componentDidMount() {
    console.log(this.props)
  }

  state = {
    collapsed: false,
  };

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };

  render() {
    return (
      <div style={{backgroundColor: 'white'}}>
        <Image src="img/q1e.jpg"/>
        <br/>
        <br/>
        <Row className="mt-5">
          <Col push={1} span={11}>
            <Typography>
              <Title>
                About
                About Conference
                Why the world of academia picks this social sciences conference
              </Title>
              <Paragraph>
                The 3rd International Conference on Advanced Research in Social Sciences (ICARSS) invites members of
                the
                academia, scholars, and researchers to Oxford – United Kingdom on 11 – 14 of March, 2021. Join social
                sciences conference as we explore the latest trends in the field of social sciences and discuss common
                challenges in the areas of politics, law, education, humanities, culture studies, society, and
                sociology.
                Immerse yourself in the pool of knowledge and share your research findings with the international
                network.
                We are currently accepting paper and poster submissions, so make sure to add important deadlines to
                your
                calendar.
              </Paragraph>
            </Typography>
            <Button size="large" onClick={() => router.push('/event/about')}
                    style={{borderRadius: 50, borderColor: 'darkblue'}}>Read More</Button>
          </Col>
          <Col span={10} push={2}>
            <Image src="img/q11.jpg"/>
          </Col>
        </Row>
        <Divider/>
        <Row className="mt-3">
          <Col push={1} span={8}>
            <Image src="img/q12.jpg"/>
          </Col>
          <Col push={2} span={10}>
            <Typography>
              <Title>First Round History</Title>
              <Paragraph>
                The International Conference on Advanced Research in Social Sciences (ICARSS) is the industry’s
                leading
                event, with a history of success. Its 2019 edition took place on the 7th-9th of March, 2019 at the
                University of London in London, United Kingdom.
              </Paragraph>
            </Typography>
            <Button size="large" style={{borderRadius: 50, color: 'darkblue'}}>Read More</Button>
          </Col>

        </Row>
        {/*//History*/}
        <Row className="mt-5">
          <Col push={1} span={11}>
            <Typography>
              <Title>Second Round History</Title>
              <Paragraph>
                We are delighted to report that the 2019 edition of the International Conference on Advanced Research
                in Social Sciences – ICARSS – exceeded our and attendees’ expectations. Hosted on the 22nd-24th of
                November, 2019, it was the 2nd year we organized the event. This time it took place in the magnificent
                city of Paris, France.
              </Paragraph>
            </Typography>
            <Button size="large" style={{borderRadius: 50, borderColor: 'darkblue'}}>Read More</Button>
          </Col>
          <Col push={4} span={8}>
            <Image src="img/q12.jpg"/>
          </Col>
        </Row>
        <div className="text-center mt-5">
          <Typography className="ml-3 mt-5">
            <Row>
              <Col push={2} span={18}>
                <Title>
                  <Text strong>
                    Important dates to mark in your social sciences conferences calendar
                  </Text>
                </Title>
              </Col>
            </Row>
          </Typography>
        </div>
        <Row>
          <Col className="ml-3" span={5}>
            <Card style={{background: '#00035e', height: '30vh'}}>
              <div className="text-center">
                <AccountBookOutlined className="mt-5" style={{fontSize: '40px', color: 'white'}}/>
              </div>
            </Card>
          </Col>
          <Col className="ml-3" span={6}>
            <Card style={{background: '#00035e', height: '30vh'}}>
              <div className="text-center">
                <Typography className="mt-3">
                  <Text className="txtSize" style={{color: 'white'}}>
                    Paper Submission DeadLine
                    <br/>
                    <br/>
                    25 October 2021
                  </Text>
                </Typography>
              </div>
            </Card>
          </Col>
          <Col className="ml-3" span={6}>
            <Card style={{background: '#00035e', height: '30vh'}}>
              <Typography className="mt-3">
                <Text className="txtSize text-center" style={{color: 'white'}}>
                  Registration Deadline
                  <br/>
                  <br/>
                  02 December 2021
                </Text>
              </Typography>
            </Card>
          </Col>
          <Col className="ml-3" span={5}>
            <Card style={{background: '#00035e', height: '30vh'}}>
              <div className="text-center">
                <ClockCircleOutlined className="mt-5" style={{fontSize: '40px', color: 'white'}}/>
              </div>
            </Card>
          </Col>
        </Row>
        {/*//DateTable*/}
        <Row className="text-center timeTable mt-5">
          <Col span={24}>
            <h2 className="txtTime mt-3">Conference Dates:11-18-May, 2021</h2>
            <Divider style={{backgroundColor: 'white', height: '3px'}}/>
            <Row>
              <Col span={24}>
                {/*<FlipNumbers height={35} width={100} color="white" background="darkorange" play perspective={300}*/}
                {/*             numbers="12:15:45:56"/>*/}
                {/*<br/>*/}
                <h2 className="timeDays">13 : 15 : 45 : 36</h2>
                <h2 className="timeTable mr-5" style={{color: 'white'}}>Day Hou Min Sec</h2>
              </Col>
            </Row>

          </Col>
        </Row>

        <Divider>
          <p className="timeDays" style={{color: 'black'}}>News</p>
        </Divider>

        <div className="d-flex justify-content-center">
          <Row>
            <Col pull={1} span={8}>
              <Card
                className="newsCard"
                onClick={() => router.push('/news')}
                hoverable
                cover={<img src="img/q15.jpg"/>}
              >
                <Meta title="Conference Schedule"/>
              </Card>
            </Col>
            <Col span={8}>
              <Card
                className="newsCard"
                onClick={() => router.push('/news')}
                hoverable
                cover={<img src="img/q15.jpg"/>}
              >
                <Meta title=" Modes of Presentation"/>
              </Card>
            </Col>
            <Col push={1} span={8}>
              <Card
                className="newsCard"
                onClick={() => router.push('/news')}
                hoverable
                cover={<img src="img/q15.jpg"/>}
              >
                <Meta title="Modes of Presentation"/>
              </Card>
            </Col>
          </Row>
        </div>

      </div>
    )
      ;
  }
}


export default Home;
