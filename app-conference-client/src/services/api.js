export default {

  signUp: 'POST /auth/register',
  signIn: 'POST /auth/login',
  editUser: 'PUT /auth',


  getCountry: ' /country',
  getCountries: ' /country',
  addCountry: 'POST /country',
  editCountry: 'PUT /country',
  deleteCountry: 'DELETE /country',

  getPosition: ' /position',
  getPositions: ' /position',

  addRegion: 'POST /region',
  editRegion: 'PUT /region',
  deleteRegion: 'DELETE /region',
  getRegions: '/region',
  getRegionByCountry: '/region/search/filterByCountry',

  getDistricts: '/district',
  addDistrict: 'POST /district',
  editDistrict: 'PUT /district',
  deleteDistrict: 'DELETE /district',
  getDistrictsByRegion: '/district/search/filterByRegion',

  getPhoneNumberTypes: '/phoneNumberType',

  getUniversities: '/university',

  getAwares: '/aware',

  uploadFile: 'POST /attach',

  addEvent: 'POST /event',
  getEvents: '/event/getMyEvents',
  deleteEvent: 'DELETE /event'
}

