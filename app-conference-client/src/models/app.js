import api from "api";
import {config} from "utils"
import {router} from "umi";
import {toast} from "react-toastify";

const {
  getCountries, getPositions, getRegions, getRegionByCountry,
  getDistrictsByRegion, getPhoneNumberTypes, getUniversities,
  getAwares, getDistricts, uploadFile, addEvent, getEvents
} = api


export default {
  namespace: 'app',
  state: {
    collapsed: false,
    countries: [],
    positions: [],
    regions: [],
    districts: [],
    phoneNumberTypes: [],
    universities: [],
    awares: [],
    photosId: [],
    videosId: [],
    photosList: [],
    logoId: '',
    videoList: [],
    typeName: '',
    events: []
  },
  subscriptions: {},
  effects: {
    * getCountries({payload}, {call, put, select}) {
      const res = yield call(getCountries);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            countries: res._embedded.countries
          }
        })
      }
    },
    * getRegions({payload}, {call, put}) {
      const res = yield call(getRegions);
      if (res.success) {
        console.log(res)
        yield put({
          type: 'updateState',
          payload: {
            regions: res._embedded.regions
          }
        })
      }
    },
    * getDistricts({payload}, {call, put}) {
      const res = yield call(getDistricts);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            districts: res._embedded.districts
          }
        })
      }
    },
    * getPositions({payload}, {call, put}) {
      const res = yield call(getPositions);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            positions: res._embedded.positions
          }
        })
      }
    },
    * getRegionByCountry({payload}, {call, put}) {
      const res = yield call(getRegionByCountry, payload)
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            regions: res._embedded.regions
          }
        })
      }
    },
    * getDistrictsByRegion({payload}, {call, put}) {
      const res = yield call(getDistrictsByRegion, payload)
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            districts: res._embedded.districts
          }
        })
      }
    },
    * getPhoneNumberTypes({payload}, {call, put}) {
      const res = yield call(getPhoneNumberTypes);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            phoneNumberTypes: res._embedded.phoneNumberTypes
          }
        })
      }
    },
    * getUniversities({payload}, {call, put}) {
      const res = yield call(getUniversities)
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            universities: res._embedded.universities
          }
        })
      }
    },
    * getAwares({payload}, {call, put}) {
      const res = yield call(getAwares)
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            awares: res._embedded.awares
          }
        })
      }
    },
    * uploadFile({payload}, {call, put}) {
      return yield call(uploadFile, {payload});
    },
    * addEvent({payload}, {call, put}) {
      console.log(payload);
      const res = yield call(addEvent, payload);
      if (res.success) {
        router.push('/home')
        toast.success(res.message);
      }
    },
    * getEvents({payload}, {call, put}) {
      const res = yield call(getEvents);
      if (res.success) {
        console.log(res)
        yield put({
          type: 'updateState',
          payload: {
            events: res.object
          }
        })
      }
    }

  },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
}
