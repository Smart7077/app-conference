module.exports={
  CORS:[],
  apiPrefix: '/api',
  openPages:['/auth/login','/auth/register','/'],
  userPages:['/user/edit','/user/editPassword', '/faq'],
  cashierPages:['/catalog/payType','/catalog/position',
    '/catalog/tariff','/catalog/voucher','/client','/expense','/payment','/voucher']
};
