import firebase from 'firebase/app';
import 'firebase/auth';

const prodConfig = {
  apiKey: "AIzaSyCBrC2IDWShHc0VlaMDi1pxVDpnfVnQN3A",
  authDomain: "app-tour-a19c7.firebaseapp.com",
  databaseURL: "https://app-tour-a19c7.firebaseio.com",
  projectId: "app-tour-a19c7",
  storageBucket: "app-tour-a19c7.appspot.com",
  messagingSenderId: "951798918980",
  appId: "1:951798918980:web:29c36918cda480cad7f111",
  measurementId: "G-3WGJJJ428F"
};

const devConfig = {
  apiKey: "AIzaSyCBrC2IDWShHc0VlaMDi1pxVDpnfVnQN3A",
  authDomain: "app-tour-a19c7.firebaseapp.com",
  databaseURL: "https://app-tour-a19c7.firebaseio.com",
  projectId: "app-tour-a19c7",
  storageBucket: "app-tour-a19c7.appspot.com",
  messagingSenderId: "951798918980",
  appId: "1:951798918980:web:29c36918cda480cad7f111",
  measurementId: "G-3WGJJJ428F"
};

const config = process.env.NODE_ENV === 'production'
  ? prodConfig
  : devConfig;

if (!firebase.apps.length) {
  firebase.initializeApp(config);
}

const firebaseAuth = firebase.auth();

export {
  firebaseAuth, firebase
};
