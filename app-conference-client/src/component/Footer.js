import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Col, Form, Input, Row, Space} from "antd";
import Text from "antd/es/typography/Text";

class Footer extends Component {
  render() {
    return (
      <div className="mt-5 text-center" style={{backgroundColor: "#616161"}}>
        <Row>
          <Col push={1} span={8}>
            <h3 className="mt-3" style={{color: 'white'}}>Letter</h3>
            <Form wrapperCol={{span: '15'}}>
              <Form.Item>
                <Input placeholder="Full Name"/>
              </Form.Item>
              <Form.Item>
                <Input placeholder="Email"/>
              </Form.Item>
            </Form>
          </Col>
          <Col span={8}>

          </Col>
          <Col span={8}>
            <h4 className="font-weight-bold mt-3" style={{color: 'white'}}>Contact Us</h4>
            <Space direction="vertical">
              <Text style={{color: 'white', float: 'left'}} strong>Phone Number</Text>
              <Text className="footerContact" style={{color: 'white', float: 'left'}}>+998 (90) 456 4852 965</Text>
              <Text style={{color: 'white', float: 'left'}} strong>Email</Text>
              <Text className="footerContact" style={{color: 'white', float: 'left'}}>allconferenceuzb@gmail.com</Text>
            </Space>
          </Col>
        </Row>
      </div>
    );
  }
}

Footer.propTypes = {};

export default Footer;
